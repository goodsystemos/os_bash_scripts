#!/bin/bash
function hardware.conf {
clear
./hardware.sh
}
function  load {
        clear
        echo " _________ "
        echo "|^|     | |"
        echo "| |_____| |"
        echo "|  _____  |"
        echo "| |D____| |"
        echo "| |_____| |"
        echo "|_|_____|_|"
        sleep 0.2
        clear
        echo " _________ "
        echo "|^|     | |"
        echo "| |_____| |"
        echo "|  _____  |"
        echo "| |DI___| |"
        echo "| |_____| |"
        echo "|_|_____|_|"
        sleep 0.2
        clear
        echo " _________ "
        echo "|^|     | |"
        echo "| |_____| |"
        echo "|  _____  |"
        echo "| |DIS__| |"
        echo "| |_____| |"
        echo "|_|_____|_|"
        sleep 0.2
        clear
        echo " _________ "
        echo "|^|     | |"
        echo "| |_____| |"
        echo "|  _____  |"
        echo "| |DISK_| |"
        echo "| |_____| |"
        echo "|_|_____|_|" 
        sleep 0.2
        clear
        echo " _________ "
        echo "|^|     | |"
        echo "| |_____| |"
        echo "|  _____  |"
        echo "| |L____| |"
        echo "| |_____| |"
        echo "|_|_____|_|"
        sleep 0.2
        clear
        echo " _________ "
        echo "|^|     | |"
        echo "| |_____| |"
        echo "|  _____  |"
        echo "| |LO___| |"
        echo "| |_____| |"
        echo "|_|_____|_|"
        sleep 0.2
        clear
        echo " _________ "
        echo "|^|     | |"
        echo "| |_____| |"
        echo "|  _____  |"
        echo "| |LOA__| |"
        echo "| |_____| |"
        echo "|_|_____|_|"
        sleep 0.2
        clear
        echo " _________ "
        echo "|^|     | |"
        echo "| |_____| |"
        echo "|  _____  |"
        echo "| |LOAD_| |"
        echo "| |_____| |"
        echo "|_|_____|_|" 
        sleep 0.5
}
function meminfo {
clear
/bin/ cat /proc/meminfo
}
function datastorage {
clear
ranger /home
}
function echo1 {
clear
echo "1"
}
function menu2 {
./yap2.sh
}
function test {
./test.sh
}
function  breakfunc {
        clear
        read -p 'Вы действительно хотите выйти? y/n -> ' vyhod
        if [[ $vyhod == y ]]; then
        exit
        else
        echo "Возврат в меню"
        fi
}
#menu visual
function menu {
clear
echo
echo -e "\t\t                           __                       "               
echo -e "\t\t                          /\ \                      "              
echo -e "\t\t    __     ___     ___    \_\ \                     "             
echo -e "\t\t  /'_ '\  / __'\  / __'\  /'_' \                    "            
echo -e "\t\t /\ \I\ \/\ \T\ \/\ \N\ \/\ \O\ \                   "           
echo -e "\t\t \ \____ \ \____/\ \____/\ \___,_\                  "          
echo -e "\t\t  \/___W\ \/___/  \/___/  \/__,_ /                  "         
echo -e "\t\t    /\____/                                         "        
echo -e "\t\t    \_/__/                                          "       
echo -e "\t\t                        __                          "      
echo -e "\t\t                       /\ \__                       "     
echo -e "\t\t   ____  __  __    ____\ \ ,_\    __    ___ ___     "    
echo -e "\t\t  /',__\/\ \/\ \  /',__\\ \ \/  /'__'\/' __' __'\   "  
echo -e "\t\t /\__, '\ \ \_\ \/\__, '\\ \ \_/\  __//\ \/\ \/\ \  "
echo -e "\t\t \/\____/\/'____ \/\____/ \ \__\ \____\ \_\ \_\ \_\ "
echo -e "\t\t  \/___/  '/___/> \/___/   \/__/\/____/\/_/\/_/\/_/ "
echo -e "\t\t             /\___/                                 "
echo -e "\t\t             \/__/                                  "   
echo -e "\t\t                                        version 1.0.0 " 
echo -e "\t\t                                         " 
echo -e "\t\t                                      " 
                      D=$(date  +%Y-%m-%d/%A)
                       T=$(date +%H:%M)
echo          "                                                     Date/Time $D" "$T"
echo -e "\t\t                                         " 
echo -e "\t\t\t Командное Меню Терминала\n"
echo -e "\t\t                                         " 
echo -e "\t1. Configuration (Конфигурация системы)"
echo -e "\t2. Arch command terminal (Командный терминал АРЧ)"
echo -e "\t3. Data storage - to exit q (Файловый менджер - выход q)"
echo -e "\t4. Featchures (Инструменты)"
echo -e "\t5. Network features (Инструменты требующие интернет)"
echo -e "\t6. Media (Медиа ресурсы) "
echo -e "\t7. Тестовый вызов операции (Инструмент для тестирования) "
echo -e "\t8. Knoweledge base (База знаний)"
echo -e "\t9. Документация "
echo -e "\t0. Выход"
echo -en "\t\t Ожидание ввода команды: "
read -n 1 option
}
#menu software
while [ $? -ne 1 ]
do
        menu
        case $option in
0)
        breakfunc
        ;;
1)
        load
        clear
        ./hardware.conf.sh ;;
2)
        echo " Это командная строка эмулятора терминала ARCH, в случае если вы 
        знаете команды, можете попробовать ввести их тут. Для вызова справки введите help"
        echo -en "\n\n\t\t\t Пожалуйста нажмите любую клавишу для продолжения"
        sleep
        clear
        $info1
        read -p 'Введите команду ->' info1
        $info1 ;;
3)
        datastorage ;;
4)
        echo  ;;
5)
        viewpon ;;
6)
        menu2 ;;
7)
        test ;;
a)

        ;;
        
*)
        clear
echo "Неверный ввод данных, пожалуйста повторите ввод";;
esac
echo -en "\n\n\t\t\t Пожалуйста нажмите любую клавишу для продолжения"
read -n 1 line
done
clear
